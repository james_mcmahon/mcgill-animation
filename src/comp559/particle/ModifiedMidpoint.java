package comp559.particle;

public class ModifiedMidpoint implements Integrator {

    @Override
    public String getName() {
        return "modified midpoint";
    }

    @Override
    public void step(double[] y, int n, double t, double h, double[] yout, Function derivs) {
    	double[] dydt = new double[n];
        double[] yMid = new double[n];
        derivs.derivs(t, y, dydt);
        int count = 0;
        while ( count < n ){ // for each pos or vel variable
        	yMid[count] = y[count] + h * dydt[count]/2.0; //apply the forward Euler
        	count++; // move to next variable
        }
        
        derivs.derivs(t + h/2.0, yMid, dydt);
        // TODO convert this to the modified method
        count = 0;
        while ( count < n ){ // for each pos or vel variable
        	yout[count] = y[count] + h * dydt[count]; //apply the forward Euler
        	count++; // move to next variable
        }
    }

}
