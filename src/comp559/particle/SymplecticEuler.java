package comp559.particle;

public class SymplecticEuler implements Integrator {

    @Override
    public String getName() {
        return "symplectic Euler";
    }

    @Override
    public void step(double[] y, int n, double t, double h, double[] yout, Function derivs) {
        // TODO Auto-generated method stub
    	double[] dydt = new double[n];
        derivs.derivs(t, y, dydt);
        
        int count = 0;
        while ( count < n ){ // for each pos or vel variable
        	count+=2;
        	y[count] = y[count] + h * dydt[count];
        	count++; // move to next variable
        	y[count] = y[count] + h * dydt[count];
        	count++; // move to next variable
        }
        derivs.derivs(t, y, dydt);
        
        count = 0;
        while ( count < n ){ // for each pos or vel variable
        	yout[count] = y[count] + h * dydt[count]; //apply the forward Euler
        	count++; // move to next variable
        }
        
    }

}
