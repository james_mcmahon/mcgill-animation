package comp559.particle;

public class RK4 implements Integrator {
    
    @Override
    public String getName() {
        return "RK4";
    }

    @Override
    public void step(double[] y, int n, double t, double h, double[] yout, Function derivs) {
    	double[] yWork = new double[n];
    	double[] k1 = new double[n];
    	double[] k2 = new double[n];
    	double[] k3 = new double[n];
    	double[] k4 = new double[n];
    	
        derivs.derivs(t, y, k1);
        int count = 0;
        while ( count < n ){ // for each pos or vel variable
        	yWork[count] = y[count] + k1[count]* h / 2.0;
        	count++; // move to next variable
        }
        
        derivs.derivs(t + h/2.0, yWork, k2);
        count = 0;
        while ( count < n ){ // for each pos or vel variable
        	yWork[count] = y[count] + k2[count]* h / 2.0;
        	count++; // move to next variable
        }
        
        derivs.derivs(t + h/2.0, yWork, k3);
        count = 0;
        while ( count < n ){ // for each pos or vel variable
        	yWork[count] = y[count] + k3[count]* h;
        	count++; // move to next variable
        }
        derivs.derivs(t + h/2.0, yWork, k4);
        count = 0;
        while ( count < n ){ // for each pos or vel variable
        	yout[count] = y[count] + h / 6.0 *
        			( k1[count] +
					2 * k2[count] + 
					3 * k3[count] + 
					k4[count]);
        	count++; // move to next variable
        }
    }
}
